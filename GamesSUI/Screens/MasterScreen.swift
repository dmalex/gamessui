//
//  MasterScreen.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import SwiftUI

struct MasterScreen: View
{
    @StateObject private var viewModel = ViewModel()
    var body: some View {
        NavigationView {
            List(viewModel.games) { item in
                NavigationLink(destination: DetailsScreen(item: item)) {
                    HStack(spacing: 15) {
                        ImageView(url: item.thumb)
                        TextsView(item: item, hasTitle: true)
                    }
                }
            }
            .onAppear() {
                
                viewModel.getData()
            }
            .navigationTitle(viewModel.title)
        }
    }
}
