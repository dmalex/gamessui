//
//  DetailsScreen.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import SwiftUI

struct DetailsScreen: View
{
    var item: Game
    var body: some View {
        VStack(spacing: 15) {
            HStack {
                ImageView(url: item.thumb, isWide: true)
            }
            TextsView(item: item, hasTitle: false)
            Spacer()
        }
        .navigationTitle(item.title)
        .padding()
    }
}
