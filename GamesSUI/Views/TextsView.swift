//
//  TextsView.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import SwiftUI

struct TextsView: View
{
    var item: Game
    var hasTitle: Bool
    var body: some View {
        VStack(alignment: .leading) {
            if hasTitle {
                Text("\(item.title)").bold().lineLimit(1)
            }
            Text("Originality: \(item.normalPrice)$")
            Text("Discounted: \(item.salePrice)$")
                .foregroundColor(.green)
            Text("Rating: \(item.steamRatingPercent)%")
        }
    }
}
