//
//  ImageView.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import SwiftUI
import CoreGraphics

struct ImageView: View {
    var url: String
    var isWide: Bool = false
    private let screenWidth = UIScreen.main.bounds.size.width
    var body: some View {
        AsyncImage(url: URL(string: url)!) { image in
            image
                .resizable()
                .scaledToFill()
                .frame(width: screenWidth / (isWide ? 1.1 : 3), height: screenWidth / 4)
        } placeholder: {
            Color.gray.opacity(0.3)
        }
        .cornerRadius(15)
    }
}
