//
//  MasterVM.swift
//  GamesSUI
//  Created by brfsu on 26.02.2024.
//
import Foundation
import SwiftUI

class ViewModel: ObservableObject
{
    @Published var games = [Game]()

    static let url = "https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15"
    let title = "Games"
    
    func getData() {
        Api.loadData(url: ViewModel.url, completion: { games in
            self.games = games
        })
    }
    
}
