//
//  GameSUIapp.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import SwiftUI

@main
struct GamesSUI: App
{
    var body: some Scene {
        WindowGroup {
            MasterScreen()
        }
    }
}
