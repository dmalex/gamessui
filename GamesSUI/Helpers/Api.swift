//
//  Api.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
import Foundation

class Api {
    static func loadData(url: String, completion: @escaping ([Game]) -> ()) {
        guard let url = URL(string: url) else {
            print("Incorrect url.")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            do {
                let games = try JSONDecoder().decode([Game].self, from: data!)
                DispatchQueue.main.async {
                    completion(games)
                }
            } catch {
                print("Decoding error.")
            }
        }
        .resume()
    }
}
