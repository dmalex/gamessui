//
//  Game.swift
//  GamesSUI
//  Created by brfsu on 24.01.2024.
//
// GET (list of deals):
// https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15
//
import Foundation

struct Game: Codable, Identifiable
{
    let id = UUID() // let!
    var title: String
    var normalPrice: String
    var salePrice: String
    var steamRatingPercent: String
    var thumb: String
}
